# Camera Tracking Training Resources

## Author(s): Naren Kasinath

## Overview
The purpose of this repository is to provide training resources for OpenCV and vision processing for use in the FIRST Robotics Competition.

OpenCV is a powerful, open-source library for computer vision processing that has many capabilities; for example, one can design an algorithm to recognize and track objects from video feeds.

OpenCV has support for traditional languages, such as Java and C++; however, Python will be used for these training resources due to Python's simple syntax and high level of applicability in scientific computing.

The goal of this training program is to introduce basic Python syntax and slowly build up to more complex vision processing applications.
## Resources Required
- A Windows or Unix machine
- The latest version of Python 3
- OpenCV for Python
- A USB Webcam
## Installation Procedure (Windows)
- Download the latest version of Python 3 from the official website (https://www.python.org/)
	- Python 3 should automatically come packageed with pip, the package installer. This will prove essential in the next few steps.
	- IMPORTANT: There is an option to "check" adding Python to the Windows PATH. To properly use Python and pip with the Windows terminal, YOU MUST SELECT THIS OPTION.
- In the terminal, test to see if Python has properly been configured by running "python" (without quotes) in the Windows Command Terminal.
	- Exit out of Python using "quit()"
- Test pip by running "pip"
- Installing OpenCV
	- Install numpy by running "pip install numpy"
		- Numpy is a scientific computing library and also has the capability of parsing C++ code to Python and vice-versa. Many of the core libraries for OpenCV-Python are written in C++.
	- Install OpenCV by running "pip install opencv-python"
## Basic Usage
- You can write Python code by opening the Python IDLE application, going to the File tab, and opening a new file.
	- You can run your code by clicking "Run Module" under the "Run" tab
## General Documentation
- OpenCV-Python: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html
- Numpy: https://docs.scipy.org/doc/numpy/dev/
- Python: https://docs.python.org/3/