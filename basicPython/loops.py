#This program demonstrates loop structures

#For loops start from the initial value and go up to but not including the limit value.
#By default, for loops increment the counter by one for each iteration.
#The following example starts with i = 0 and ends with i = 2; the condition will run three times.
for i in range(0,3):
    print("I have " + str(i) + " batteries")
x = 1
while (x != 10):
    print("I have " + str(x) + " more batteries")
    #While loops do not auto-increment by default since they do not always use counters; in this case the counter must be manually incremented
    x += 1
#Loops can be nested if necessary
for a in range (0, 10):
    #b starts at b = 1 and ends at b = 5
    for b in range (1, 6):
        print(a * b)