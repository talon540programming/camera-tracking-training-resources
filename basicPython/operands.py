#This program demonstrates basic conditions for if/else statements

#To get user input, use the 'input()' command; use 'int()' to convert the raw input (a string) into an integer
myAge = int(input("What is your Age? "))


#An if statement compares a given value to another
#Operators
#	 > -- Greater than
#	 < -- Less than
#	== -- Equals
#	!= -- Does not equal
#	>= -- Greater than or equal to
#	<= -- Less than or equal to

if(myAge == 0):
    print("You're lying.")

#elif allows you to perform another comparison if the previous one registered as false
#Using the 'and' clause requires that both conditions must be true
elif(myAge > 0 and myAge < 10):
    print("You are young.")

#Conditions can be nested if necessary
#Using the 'or' clause only requires that one of the conditions must be true
elif((myAge > 10 and myAge < 18) or myAge < 21):
    print("You cannot consume alcohol in the United States of America.")

#Else is the final fallback statement that is run when all other statements are not satisfied
else:
    print("You are an adult.")